So to start this make sure you have the latest release of node 7.5^ and run
```sh
npm install
```
Then install postgres and set that up
link: http://www.postgresqltutorial.com

and run 
```sh
psql -f habits.sql

```

**MAKE SURE POSTGRES IS RUNNING IF YOU WANT TO USE THE DATABASE**

And then to start in debug mode
```sh
DEBUG=* npm start
```

or 

```sh
DEBUG=* ./bin/www
```

or on windows use 

```sh
set DEBUG=* & npm start;
```

Let me know if you need help understanding things. We can skype sometime soon and put everything together