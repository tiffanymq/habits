var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var db = require('./db_management/queries');

var index = require('./routes/index');
var main = require('./routes/main');
var session = require('express-session');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
var sess = {
  secret: 'fb9b15b6c3e7380a5cf9f64d69e04904',
  resave: false,
  saveUninitialized: false,
  cookie: {}
};
if (app.get('env') === 'production') {
  app.set('trust proxy', 1); // trust first proxy
  sess.cookie.secure = true;// serve secure cookies
}
app.use(session(sess));
app.use(require('node-sass-middleware')({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true,
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
//Login check
//app.post('/findHabits',db.findHabits);
app.use( function(req, res, next){

  if(req.session.user){

    res.locals.user = req.session.user;

    next();
  }else{
    res.redirect('/');
  }



});
app.use('/main', main);
app.use('/logout',require('./routes/logout'));
//app.use('/test', db.test);
//app.use('/loginTest', db.authenticateUser);

app.get('/favicon.ico', function(req, res) {
    res.sendStatus(204);
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
