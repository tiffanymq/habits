/*
	This is to make it easier to build the add user query
	author:Armond St.Juste
 */

INSERT into users (email, password, active)
	VALUES(${email}, ${pass}, ${active})