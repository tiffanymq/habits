/*
	This is just the initial setup of the database, I'd probably need to be alot more thorough if this was production stuff
	Let me know if you need any explination 
	author:Armond St.Juste
 */

DROP DATABASE IF EXISTS habits;
CREATE DATABASE habits;


\c habits;

CREATE EXTENSION pgcrypto;

CREATE TABLE users (
id uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
email text NOT NULL UNIQUE,
password text NOT NULL,
created_at timestamp NOT NULL DEFAULT now(),
modified_at timestamp NOT NULL DEFAULT now(),
active boolean NOT NULL

);

CREATE TABLE habits (
id uuid NOT NULL DEFAULT gen_random_uuid() PRIMARY KEY,
user_email text NOT NULL,
description text NOT NULL,
start_date date NOT NULL DEFAULT current_date,
end_date date NOT NULL DEFAULT current_date + integer '21',
created_at timestamp NOT NULL DEFAULT now(),
modified_at timestamp NOT NULL DEFAULT now(),
active boolean NOT NULL
);

INSERT INTO users (email, password, active) 
	VALUES ('test@test.com', 'apples', 't');

INSERT INTO habits (user_email, description, active)
	VALUES ('test@test.com', 'Sleep 8hrs', 't');

CREATE OR REPLACE FUNCTION update_modified()
RETURNS TRIGGER AS $$
BEGIN
	IF row(NEW.*) IS DISTINCT FROM row(OLD.*) THEN
		NEW.modified_at = now();
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_mod
	AFTER UPDATE 
	ON users
	EXECUTE PROCEDURE update_modified();

CREATE TRIGGER update_mod2
	AFTER UPDATE
	ON habits
	EXECUTE PROCEDURE update_modified();
