/*
File to hold queries to the database
Author:Armond St.Juste
 */


var promise = require('bluebird');
//This is how you use the debug tool, requre it and then send it whatever you want the filter to be
var debug = require('debug')('queries');
var options = {

	promiseLib:promise
};
var pgp = require('pg-promise')(options);
var connectionString = 'postgres://localhost:5432/habits';
var db = pgp(connectionString);


//lniker for sql files
function sql(file){
	return new pgp.QueryFile(file,{minify: true});
}
// I wanted to test out using query files here so I made one to find a user
// This is me importing that to use next
var sqlFindUser = sql('../db_management/findUser.sql');
var sqlAddUser = sql('../db_management/addUser.sql');
var sqlfindHabits = sql('../db_management/findHabits.sql');
/*
	This is the function to authenticat a user
	it returns success and the data if true, and then returns false and the error otherwise
 */
function authenticateUser(req,res,next){
	//Lets get the email and password
	//req is the web request coming in
	//res is the response to send back
	//and next is the callback function in case I want to handle things later
	var email = req.body.email.toLowerCase();
	debug(email);
	var password = req.body.password;
	//This is sending the password to the debug console
	debug(password);
	//This is a request to the database
	//the "one" signifies that its expecting one row of information back
	//and it returns a promise that I handle with .then and .catch
	db.one(sqlFindUser,{
		email: email,
		pass: password,
		active: true
	})
	.then(function(data){
		debug(data);
		/*res.status(200)
		.json({
			status: 'success',
			data:data,
			message: 'User exists!'
		});
		*/
		var response = {success:true, data:data};
		return next(response);
	})
	.catch(function(err){
		var response = {success:false, error:err};
		return next(response);
	});
}

function removeUser(req,res,next){
	console.log('Removing user(not really yet');
}

function updateUser(req,res,next){
	console.log('Updating user(at some point)');
}

function insertUser(req,res,next){
	var email = req.body.email.toLowerCase();
	debug(email);
	var password = req.body.password;
	debug(password);

	db.none(sqlAddUser,{
		email: email,
		pass: password,
		active: true
	})
	.then(function(){
		db.one(sqlFindUser,{
			email: email,
			pass: password,
			active: true
		})
		.then(function(data){
			debug(data);
			var response = {success:true, data:data};
			return next(response);
		})
		.catch(function(err){
			var response = {success:false, error:err};
			return next(response);
		});
	})
	.catch(function(err){
		var response = {success:false, error:err};
		return next(response);
	});

}

function findHabits(req,res,next){
	var user_email = req.body.user_email.toLowerCase();
	var active = req.body.active;
	debug(user_email);
	db.many(sqlfindHabits,{
		user_email: user_email,
		active: active		
	})
	.then(function(data){

		debug(data);
		var response = {success:true,data:data};
		res.status(200).json({
			status:'success',
			data:data,
			message:'Test Works'
		});
		//return next(response);
	})
	.catch(function(err){
		var response = {success:false,error:err};
		return next(response);
	});
}

//Some test stuffs
/*
function test(req,res,next){
	db.any('select * from users')
	.then(function(data){
		res.status(200)
		.json({
			status: 'success',
			data: data,
			message: 'Test workss'
		});
	})
	.catch(function(err){
		return next(err);
	});
}
*/
module.exports = {
	authenticateUser: authenticateUser,
	removeUser: removeUser,
	updateUser: updateUser,
	addUser: insertUser,
	findHabits: findHabits
};
