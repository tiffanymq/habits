$(document).ready(function(){
    $('#signup-form').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            pw: {
                required: true,
                minlength: 5,
            },
            pwAgain: {
                    required: true,
                    equalTo: '#pw',
            }
            
        },
        messages: {
            email: {
                required: "Enter your email",
                email: "Not a valid email",
            },
            pw: {
                required: "Enter in a password",
                minlength: "Must be at least 5 characters"
            },
            pwAgain: {
                required: "Re-enter your password",
                equalTo: "Passwords have to match"
            },
        },
             highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
        },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
        },
    });

    $('#login-form').validate({
        rules: {
            email: {
                required: true,
                email: true,
            },
            password: {
                required: true,
            }
        },
        messages: {
            email: {
                required: "Enter your email",
                email: "Not a valid email",
            },
            password: {
                required: "Enter in a password",
            },
        },
             highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
        },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
        },
    });
});

