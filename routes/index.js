var express = require('express');
var router = express.Router();
var debug = require('debug')('index');
var db = require('../db_management/queries');

/* GET home page. */
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });
  initSession(req);
  showMainPage(req ,res);
});



router.post('/login', function(req,res,next){

	if(!req.body.email){
		// Reload the main page here
		console.log('No eemail, cant login without it');
		//showMainPage(req, res);
		req.session.loginError = "No eemail, cant login without it";
		res.redirect('/');
		return;
	}
	if(!req.body.password){
		//Reload the main page
		console.log('There is no password, need one of those');
		//showMainPage(req, res);
		req.session.loginError = 'There is no password, need one of those';
		res.redirect('/');
		return;
	}

	//Im not sure if I have to do anything with this in terms of extra error
	//handling
	debug('we in here');
	db.authenticateUser(req, res, function(response){
		debug(response);
		if(response.success){
			debug(response.data);
			debug(response.data.email + "Logged in succesfully");
			//showMainPage(req,res, response.data.email + " Logged in succesfully!");
			//Gonna add the user specific session in later
			req.session.loginError = '';
			req.session.loginSuccess = {'success': true, "message": response.data.email + " Logged in succesfully!"};

			//set the user session herrreeerere
			req.session.user = {
				email: response.data.email,
				uuid: response.data.id
			};
			//This line here is telling the response to redirect to the main page(what used to be named userhomepage)
			res.redirect('/main');
			return;
		}
		else{
			debug(response.error);
			debug("User is unauthenticated");
			//res.json({success:false, error:"Incorrect email or password"});
			//showMainPage(req, res, "Wrong email or Password");
			req.session.loginError = 'Wrong email or password';
			res.redirect('/');
			return;
		}
	});
});


router.post('/signup',function(req,res,next){

	if(!req.body.email){
		// Reload the main page here
		console.log('No eemail, cant login without it');
		//showMainPage(req, res);
		req.session.loginError = "No eemail, cant login without it";
		res.redirect('/');
		return;
	}

	if(!req.body.password){
		//Reload the main page
		console.log('There is no password, need one of those');
		//showMainPage(req, res);
		req.session.loginError = 'There is no password, need one of those';
		res.redirect('/');
		return;

	}

	// Last param is an anonymous function to handle the response
	db.addUser(req, res ,function(response){
		debug(response);
		if(response.success){
			req.session.loginError = '';
			req.session.loginSuccess = {success:true, "message":response.data.email + "Logged in and Signed up!"};

			req.session.user = {
				email: response.data.email,
				uuid: response.data.id
			};

			res.redirect('/main');
			return;
		}else{
			req.session.loginError = "Something was wrong with the signup process, please try again";
			res.redirect('/');
			return;
		}
	});

});

function initSession(req){
	//debug(req);
	if(req.session.loginError === undefined  || req.session.loginSuccess === undefined){
		req.session.loginError = '';
		req.session.loginSuccess = '';
	}
}
function showMainPage(req, res){
	//debug(req.body);
	//debug(res);
	debug(req.session);
	res.render('index',{
		'title' : 'Home',
		'loginError' : req.session.loginError,
		'loginSuccess' : req.session.loginSuccess
	});

}


module.exports = router;
