var express = require('express');
var router = express.Router();
var debug = require('debug')('logout');
var db = require('../db_management/queries');


router.get('/', function(req, res, next){
    debug("were logging out now");
    debug(req.session.user);
    //May need to rethink the way I handle certain things in the session store
	delete req.session.user;
    req.session.loginSuccess = '';
     req.session.loginError = '';
     debug(req.session.user);
    

	res.redirect('/');
});


module.exports = router;