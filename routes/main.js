var express = require('express');
var router = express.Router();
var debug = require('debug')('main');

/* GET users listing. */
router.get('/', function(req, res, next) {
  showHomePage(req ,res);
});


function showHomePage( req, res){

	debug(req.session);
	res.render('main',{
		'title': 'Home',
		'email': req.session.user.email,
		'loginSuccess': req.session.loginSuccess
	});

}
module.exports = router;
